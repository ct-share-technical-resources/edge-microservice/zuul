package fr.soat.zuul.zuul.security.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.provider.OAuth2Authentication;



public class ApiAuthentication implements Authentication {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5665948288361389079L;
	
	
	private AbstractAuthenticationToken authentication;
	private Map<String,Object> details = new HashMap<>();
	
	
	@SuppressWarnings("unchecked")
	public ApiAuthentication(AbstractAuthenticationToken authentication) {
		super();
		this.authentication = authentication;
		
		if(authentication instanceof OAuth2Authentication)
		{
			details =  (Map<String,Object>) ((OAuth2Authentication)authentication).getUserAuthentication().getDetails();
		}
		
	}

	@Override
	public String getName() {
		return authentication.getName();
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authentication.getAuthorities();
	}

	@Override
	public Object getCredentials() {
		return authentication.getCredentials();
	}

	@Override
	public Map<String,Object> getDetails() {
		return details;
	}

	@Override
	public Object getPrincipal() {
		return authentication.getPrincipal();
	}

	@Override
	public boolean isAuthenticated() {
		return authentication.isAuthenticated();
	}

	@Override
	public void setAuthenticated(boolean isAuthenticated)
			throws IllegalArgumentException {
		authentication.setAuthenticated(isAuthenticated);		
	}

	

	public String getMail()
	{
		return (String)getDetails().get("email");
	}
	
}
