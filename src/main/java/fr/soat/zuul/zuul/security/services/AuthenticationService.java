package fr.soat.zuul.zuul.security.services;

import fr.soat.zuul.zuul.security.model.ApiAuthentication;

public interface AuthenticationService {

	/**
	 * @return
	 */
	ApiAuthentication getAuthentication();
	
}
