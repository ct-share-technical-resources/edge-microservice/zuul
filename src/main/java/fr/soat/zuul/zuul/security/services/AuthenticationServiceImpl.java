package fr.soat.zuul.zuul.security.services;


import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import fr.soat.zuul.zuul.security.model.ApiAuthentication;


@Component
public class AuthenticationServiceImpl implements AuthenticationService {
 

    @Override
    public ApiAuthentication getAuthentication() {
        return new ApiAuthentication((AbstractAuthenticationToken) SecurityContextHolder.getContext().getAuthentication());
    }
}
